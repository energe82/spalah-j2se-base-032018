package com.spalah.j2sebase;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        //String arg0 = args[0];
        //System.out.println("args: "+ arg0);
        System.out.println("Калькулятор:");
        double arg0 = Double.parseDouble(args[0]);
        double arg1 = Double.parseDouble(args[1]);
        String oper = args[2];
        if (oper.equals("+")) {
            System.out.println("Результат сложения " + arg0 + " + " + arg1 + " = " + (arg0 + arg1));
        } else {
            if (oper.equals("-")) {
                System.out.println("Результат вычитания " + arg0 + " - " + arg1 + " = " + (arg0 - arg1));
            } else {
                if (oper.equals("*")) {
                    System.out.println("Результат умножения " + arg0 + " * " + arg1 + " = " + (arg0 * arg1));
                } else {
                    if (oper.equals("/") & arg1 != 0) {
                        System.out.println("Результат деления " + arg0 + " / " + arg1 + " = " + (arg0 / arg1));
                    } else {
                        System.out.println("На 0 делить нельзя!!!");
                    }
                }
            }
        }
    }
}