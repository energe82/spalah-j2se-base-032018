import java.util.Random;

public class Array {
    // поля
    private int [][] array = new int[8][5];
    private int min = -50;
    private int max = 100;
    private int maxElement;
    private int minElement;
    //методы
    public void initArray() {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(max - min +1) + min;
            }
        }
    }
    public void showArray(){
        minElement = array[0][0];
        maxElement = array[0][0];
        System.out.print("№\t|");
        for (int j = 0; j < array[0].length; j++) {
            System.out.print("\t" + (j + 1) + ".\t |");
        }
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            System.out.print((i + 1) + ".\t|");
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] > maxElement){
                    maxElement = array[i][j];
                }
                if (array[i][j] < minElement){
                    minElement = array[i][j];
                }
                System.out.print("\t" + array[i][j] + "\t |");
            }
            System.out.println();
        }
    }
    public void showRes(){
        System.out.println("Максимальный элемент массива - " + maxElement);
        System.out.println("Минимальный элемент массива - " + minElement);
    }
    public void numString(){
        int proString, tmp = 0, count = 0;
        for (int i = 0; i < array.length; i++) {
            proString = 1;
            for (int j = 0; j < array[i].length; j++) {
                proString = Math.abs(proString * array[i][j]);
            }
            if (proString > tmp){
                tmp = proString;
                count = i;
            }
        }
        System.out.println("Строка с максимальным произведением элементов - № " + (count + 1));
    }
    public void numStolb(){
        int proStolb, tmp = 0, count = 0;
        for (int j = 0; j < array[0].length; j++) {
            proStolb = 1;
            for (int i = 0; i < array.length; i++) {
                proStolb = Math.abs(proStolb * array[i][j]);
            }
            if (proStolb < tmp) {
                tmp = proStolb;
                count = j;
            }
        }
        System.out.println("Столбец с максимальным произведением элементов - № " + (count + 1));
    }
    public void sortBuble(){
        for (int k = 0; k < array.length; k++) {
            for (int i = array[k].length - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (array[k][j] > array[k][j + 1]){
                        int tmp = array[k][j];
                        array[k][j] = array[k][j + 1];
                        array[k][j + 1] = tmp;
                    }
                }
            }
        }
    }
    public void sortPrint(){
        System.out.print("№\t|");
        for (int j = 0; j < array[0].length; j++) {
            System.out.print("\t" + (j +1) + ".\t |");
        }
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            System.out.print((i + 1) + ".\t|");
            for (int j = 0; j < array[i].length; j++) {
                System.out.print("\t" + array[i][j] + "\t |");
            }
            System.out.println();
        }
    }
}
