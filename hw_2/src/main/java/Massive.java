import java.lang.String;

public class Massive {
    public static void main(String[] args) {
        Array array = new Array();

        array.initArray();
        System.out.println("\tИсходный массив");
        array.showArray();
        System.out.println("\t");
        array.showRes();
        System.out.println("\t");
        array.numString();
        System.out.println("\t");
        array.numStolb();
        System.out.println("\t");
        System.out.println("\tМассив после сортировки");
        array.sortBuble();
        array.sortPrint();
    }
}
